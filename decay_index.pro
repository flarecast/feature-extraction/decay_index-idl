FUNCTION decay_index, br, binxy_di=binxy_di, di_heigh_range=di_heigh_range, binxy_mpil=binxy_mpil, mpil_info=mpil_info, mpil_xy=mpil_xy, crit_di=crit_di

;+
; Name: decay_index.pro
;
; Purpose: calculates decay index parameters using 3D magnetic field data derived from potential field extrapolation
;
; Calling Sequence:
;   output_di=decay_index(br[,binxy_di=binxy_di][,di_heigh_range=di_heigh_range][,binxy_mpil=binxy_mpil][,mpil_info=mpil_info][,mpil_xy=mpil_xy][,crit_di=crit_di])
;
; Input Parameters:
;   br - hmi_sharp_cea_720s Br or Blos image
;
; Optional Input Keyword Parameters:
;   binxy_di - number for xy spatial binning of sharp magnetic field input data related to decay index calculation [pixels] ; default=4
;   di_heigh_range - height range (MIN, MAX) for parameterising decay index [Mm] ; default=[10,50]
;   binxy_mpil - number for xy spatial binning of sharp magnetic field input data related to magnetic polarity inversion line (MPIL) detection [pixels] ; default=1
;   crit_di - critical decay index value ; default=1.5
;   mpil_info - information on magnetic polarity inversion lines (MPILs)
;   mpil_xy - x and y coordinates of MPILs [pixels]
;   
; Output Parameters:
;   output_di - 4 element 1D array related to decay index parameters             
;               output_di[0]: ratio of MPIL length to h_min for the SMPIL segment with the lowest h_min 
;               output_di[1]: ratio of MPIL length to h_min for the strong MPIL (SMPIL) segment with the longest SMPIL,
;                             where h_min is the minimum height at which decay index above the rectangle enclosing each of SMPILs is greater than the critical decay index value             
;               output_di[2]: maximum ratio of MPIL length to h_min from all SMPIL segments
;               output_di[3]: sum of ratios of MPIL length to h_min from all SMPIL segments
;
; Modificatioin History:
;           25-April-2016 - Sung-Hong Park - written
;-


IF NOT ISVALID(binxy_di) THEN binxy_di=4
IF NOT ISVALID(binxy_mpil) THEN binxy_mpil=1
IF NOT ISVALID(di_heigh_range) THEN di_heigh_range=[10.,50.]
IF NOT ISVALID(crit_di) THEN crit_di=1.5

IF (N_ELEMENTS(br[*,0])/binxy_mpil GT 8) THEN BEGIN 
IF NOT ISVALID(mpil_info) OR NOT ISVALID(mpil_xy) THEN GET_MPIL_REGION, br, mpil_info, mpil_xy, binxy=binxy_mpil

s0=SIZE(br)
br=CONGRID(br, s0[1]/binxy_di*binxy_di, s0[2]/binxy_di*binxy_di)
br=REBIN(br, s0[1]/binxy_di, s0[2]/binxy_di)

  IF FINITE(mpil_xy[0]) THEN BEGIN

    bz_height=ROUND(di_heigh_range[1]/(0.36442476*binxy_di)) ; in pixels
    B_LFF, br, FINDGEN(bz_height), bx, by, bz, /seehafer
    
    bh=1.*SQRT(bx^2+by^2) ; in G
    
    h=(0.36442476*binxy_di)*FINDGEN(bz_height) ; in Mm
    
    tmp = WHERE(FINITE(mpil_info.n) AND (mpil_info.typ LE 1), n_mpil)
    
    IF n_mpil GE 1 THEN BEGIN
    
      l_mpil=FLTARR(n_mpil)
      mean_di_mpil=FLTARR(n_mpil)
      median_di_mpil=FLTARR(n_mpil)
      min_hmin_mpil=FLTARR(n_mpil)
      l_over_min_hmin_mpil=FLTARR(n_mpil)
        
      FOR l=0, n_mpil-1 DO BEGIN
    
          mpil_x = mpil_xy[0, mpil_info[tmp[l]].offset : mpil_info[tmp[l]].offset + mpil_info[tmp[l]].n - 1]
          mpil_y = mpil_xy[1, mpil_info[tmp[l]].offset : mpil_info[tmp[l]].offset + mpil_info[tmp[l]].n - 1]
         
          di_mpil=0.
          hmin_mpil=0.
          
          x0=ROUND(MIN(mpil_x))
          x1=ROUND(MAX(mpil_x))
          y0=ROUND(MIN(mpil_y))
          y1=ROUND(MAX(mpil_y))
          nx=x1-x0+1
          ny=y1-y0+1
          l_mpil[l]=(0.36442476*binxy_mpil)*mpil_info[tmp[l]].n
          
          x=(INDGEN(FIX(ROUND(1.*nx/binxy_di*binxy_mpil))>1)+FIX(ROUND(1.*x0/binxy_di*binxy_mpil)))>0<(N_ELEMENTS(bh[*,0,0])-1)
          y=(INDGEN(FIX(ROUND(1.*ny/binxy_di*binxy_mpil))>1)+FIX(ROUND(1.*y0/binxy_di*binxy_mpil)))>0<(N_ELEMENTS(bh[0,*,0])-1)
              
          FOR i=0, N_ELEMENTS(x)-1 DO BEGIN
    
            FOR j=0, N_ELEMENTS(y)-1 DO BEGIN
    
              xx=ALOG(h[1:*])
              yy=REFORM(ALOG(bh[x[i],y[j],1:*]))
              di=(-1.)*DERIV(xx,yy)
              ss0=WHERE((h[1:*]+0.36442476*binxy_di/2.+0.36442476*binxy_di) GE di_heigh_range[0])
              di_mpil=[di_mpil,di[ss0]]
      
              ss1=WHERE((di GE crit_di) AND (h[1:*]+0.36442476*binxy_di/2.+0.36442476*binxy_di GE di_heigh_range[0]), n_ss1)
    
              IF n_ss1 GE 1 THEN BEGIN
                hmin=h[1+MIN(ss1, /nan)]+0.36442476*binxy_di/2.+0.36442476*binxy_di
                hmin_mpil=[hmin_mpil,hmin]          
              ENDIF ELSE BEGIN
                hmin_mpil=[hmin_mpil,!VALUES.F_NAN]
              ENDELSE
    
            ENDFOR
    
          ENDFOR  
          
          di_mpil=di_mpil[1:*]
          mean_di_mpil[l]=MEAN(di_mpil,/nan)
          median_di_mpil[l]=MEDIAN(di_mpil)
          
          hmin_mpil=hmin_mpil[1:*]
          ss2=WHERE(FINITE(hmin_mpil),n_ss2)
     
          IF n_ss2 GE 1 THEN BEGIN
            min_hmin_mpil[l]=MIN(hmin_mpil,/nan)
            l_over_min_hmin_mpil[l]=l_mpil[l]/min_hmin_mpil[l]
          ENDIF ELSE BEGIN
            bz_height=ROUND(di_heigh_range[1]*2./(0.36442476*binxy_di)) ; in pixels
            B_LFF, br, FINDGEN(bz_height), bx, by, bz, /seehafer
            bh=SQRT(bx^2+by^2) ; in G
            h=(0.36442476*binxy_di)*FINDGEN(bz_height) ; in Mm
            hmin_mpil=0.
       
            FOR i=0, N_ELEMENTS(x)-1 DO BEGIN
    
              FOR j=0, N_ELEMENTS(y)-1 DO BEGIN
            
                xx=ALOG(h[1:*])
                yy=REFORM(ALOG(bh[x[i],y[j],1:*]))
                di=(-1.)*DERIV(xx,yy)
                ss3=WHERE((di GE crit_di) AND (h[1:*]+0.36442476*binxy_di/2.+0.36442476*binxy_di GE di_heigh_range[0]), n_ss3)
                IF n_ss3 GE 1 THEN BEGIN
                  hmin=h[1+MIN(ss3, /nan)]+0.36442476*binxy_di/2.+0.36442476*binxy_di
                  hmin_mpil=[hmin_mpil,hmin]
                ENDIF ELSE BEGIN
                  hmin_mpil=[hmin_mpil,!VALUES.F_NAN]
                ENDELSE              
        
              ENDFOR
            
            ENDFOR  
    
            hmin_mpil=hmin_mpil[1:*]
            ss4=WHERE(FINITE(hmin_mpil),n_ss4)
            IF n_ss4 GE 1 THEN BEGIN
              min_hmin_mpil[l]=MIN(hmin_mpil,/nan)
              l_over_min_hmin_mpil[l]=l_mpil[l]/min_hmin_mpil[l]
            ENDIF ELSE BEGIN
              min_hmin_mpil[l]=-1.
              l_over_min_hmin_mpil[l]=0.
            ENDELSE
            
          ENDELSE
     
      ENDFOR
      
      s_hmin_tmp=where(min_hmin_mpil eq min(min_hmin_mpil))
      s_L_tmp=where(l_mpil eq max(l_mpil))
      maxL_over_hmin=l_over_min_hmin_mpil[s_L_tmp[0]]
      L_over_minhmin=l_over_min_hmin_mpil[s_hmin_tmp[0]]
      max_L_over_hmin=max(l_over_min_hmin_mpil, /nan)
      tot_L_over_hmin=total(l_over_min_hmin_mpil, /nan)
      
      output_di=[L_over_minhmin, maxL_over_hmin, max_L_over_hmin, tot_L_over_hmin]  
    
    ENDIF ELSE BEGIN
      
      output_di=[0.,0.,0.,0.]  
    
    ENDELSE

  ENDIF ELSE BEGIN
 
    output_di=[0.,0.,0.,0.]
  
  ENDELSE
  
ENDIF ELSE BEGIN

  output_di=[0.,0.,0.,0.]

ENDELSE

RETURN, output_di

END